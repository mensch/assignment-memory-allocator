#include "mem_internals.h"
#include "mem.h"
#include "util.h"

static struct block_header* get_header(const void * contents) {
    return (struct block_header *) ((uint8_t *) contents - offsetof(struct block_header, contents));
}

static int fail(int code){
    fprintf(stderr, "Fail\n");
    return code;
}

int main(){
    const size_t block_size = 200;
    const size_t large_block_size = 10 * block_size;
    size_t test_number = 0;


    ++test_number;
    printf("Heap initialization test\n");
    void* heap = heap_init(4321);
    if(heap){
        debug_heap(stdout, heap);
        printf("Success\n\n");
    }else{
        return fail(test_number);
    }


    ++test_number;
    printf("Regular allocation test\n");
    void* contents = _malloc(block_size);
    get_header(contents);

    struct block_header* block_header = get_header(contents);
    if (block_header -> is_free) {
        printf("Block shouldn't be free after allocation\n");
        return fail(test_number);
    }
    debug_heap(stdout, heap);
    _free(contents);
    printf("Success\n\n");


    ++test_number;
    printf("Freeing 1 block if multiple blocks are allocated test\n");
    void* contents1 = _malloc(block_size);
    void* contents2 = _malloc(block_size);
    void* contents3 = _malloc(block_size);

    debug_heap(stdout, heap);
    _free(contents2);
    debug_heap(stdout, heap);

    block_header = get_header(contents2);
    if (!block_header -> is_free) {
        printf("Block should be free after de-allocation\n");
        return fail(test_number);
    }
    printf("Success\n\n");


    ++test_number;
    printf("Freeing 2 blocks if multiple blocks are allocated test\n");
    _free(contents1);
    _free(contents3);
    debug_heap(stdout, heap);
    if ((!get_header(contents1) -> is_free) || (!get_header(contents2) -> is_free)) {
        printf("Block should be free after de-allocation\n");
        return fail(test_number);
    }
    printf("Success\n\n");


    ++test_number;
    printf("Out of memory. Heap growth test\n");
    void* contents_large = _malloc(large_block_size);
    debug_heap(stdout, heap);
    block_header = get_header(contents_large);
    if (block_header -> is_free) {
        printf("Block shouldn't be free after allocation\n");
        return fail(test_number);
    }
    printf("Success\n\n");



    printf("All tests passed\n");
    return 0;
}
